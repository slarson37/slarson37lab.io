Scott Joseph Larson
========================================

<!---
scott.joseph.larson@gmail.com
scott.larson@uga.edu
-->

<img src="./img/emails.png" height=40>  

<img src="./img/current.jpg" height=200>

[ArXiV](http://arxiv.org/a/larson_s_2)  
[MathSciNet](https://mathscinet.ams.org/mathscinet/MRAuthorID/1603704)  
[Mathematics Genealogy Project](https://www.mathgenealogy.org/id.php?id=283623)   

Papers:

<ol reversed>
<li> <a href="https://arxiv.org/abs/2203.09007">A categorification of the Lusztig--Vogan module</a> </li>
Video: <a href="https://youtu.be/wkhc-YPagDM">UGA Algebra Seminar</a>

<li> <a href="https://www.tandfonline.com/doi/full/10.1080/00927872.2023.2281602">Decompositions of Schubert varieties and small resolutions</a> </li>

<li> <a href="https://www.proquest.com/docview/2437437562">Dissertation: Small Resolutions of Closures of K-Orbits in Flag Varieties </a> </li>
Video: 
<a href="https://youtu.be/zldOj2TkGkI">UGA Algebra Seminar 1</a>
<a href="https://youtu.be/s-1b7ldllfA">UGA Algebra Seminar 2</a> 
</ol>

Videos:





[UGA Algebra Seminar Videos](http://www.youtube.com/@ugaalgebra4581)
